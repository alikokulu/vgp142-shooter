﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainMenuController : MonoBehaviour
{

	public GameObject ScoreBoard;
	public GameObject MainMenu;
	public Transform ScoreBoardContentHolder;
	public GameObject ScoreBoardRow;

	// Use this for initialization
	void Start ()
	{
		//Add 20 rows to ScoreBoard
		for (int i = 0; i < 20; i++) 
		{
			
			//Set PlayerPrefs Temporary Values
			PlayerPrefs.SetString("SBName"+i.ToString(), "Name"+i.ToString());
			PlayerPrefs.SetInt("SBScore"+i.ToString(), i * 11111);

		}

	}
	
	// Update is called once per frame
	void Update ()
	{
		if( Input.GetKey(KeyCode.Escape))
		{
			ShowMainMenu();
		}
	}


	public void StartGame ()
	{
		SceneManager.LoadScene ("Game");
	}

	public void OpenSettings ()
	{
		//Enable a different UI on the same scene
	}

	public void ShowScoreboard ()
	{
		//Disable Main Menu Panel
		MainMenu.SetActive (false);

		//Enable Scoreboard Panel
		ScoreBoard.SetActive (true);


		//Add 20 rows to ScoreBoard
		for (int i = 0; i < 20; i++) {
			//Create an instance of the ScoreRow prefab
			GameObject scoreRow = GameObject.Instantiate (ScoreBoardRow, ScoreBoardContentHolder) as GameObject;

			//Load the prefab with data
			string name = PlayerPrefs.GetString("SBName"+i.ToString());
			int score = PlayerPrefs.GetInt("SBScore"+i.ToString());

			scoreRow.GetComponent<ScoreRow> ().Load (name, score);
		}

	}


	public void ShowMainMenu ()
	{
		Debug.Log ("ESCAPE");

		//Enable Main Menu Panel
		MainMenu.SetActive (true);

		for (int i = 0; i < ScoreBoardContentHolder.childCount; i++)
		{
			Destroy (ScoreBoardContentHolder.GetChild (i).gameObject);
		}


		//Disable Scoreboard Panel
		ScoreBoard.SetActive (false);

	}

	public void QuitGame ()
	{
		Application.Quit ();
	}

}
