﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreRow : MonoBehaviour
{
	
	public void Load(string Name, int Score)
	{
		Text[] texts = GetComponentsInChildren<Text>();

		for (int i = 0; i < texts.Length; i++)
		{
			if (texts[i].gameObject.name == "Name") 
			{
				texts [i].text = Name;
			}
			else 
			{
				texts [i].text = Score.ToString();
			}
		}

	}
}
