﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;


public class Health : NetworkBehaviour
{

    public const float maxHealth = 100;
    public bool isEnemy = false;



    [SyncVar(hook = "OnChangeHealth")]
    public float currentHealth = maxHealth;


    public RectTransform healthBar;

    public override void OnStartLocalPlayer()
    {
        healthBar.transform.parent.parent.localScale /= 2;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }

    public void TakeDamage(float amount)
    {
        if (!isServer)
        {
            return;
        }

        currentHealth -= amount;
        if (currentHealth <= 0)
        {
            if (isEnemy)
            {
                Destroy(gameObject);
            }
            else
            {
                currentHealth = maxHealth;
                RpcRespawn();
            }

        }


    }

    void OnChangeHealth(float health)
    {
        healthBar.sizeDelta = new Vector2(health, healthBar.sizeDelta.y);
    }


    [ClientRpc]
    void RpcRespawn()
    {
        if (isLocalPlayer)
        {
            transform.position = Vector3.zero;
        }
    }

}
