﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Main : NetworkBehaviour
{
    private GameObject[] agents;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Escape))
        {
			Application.Quit();
		}
        
        if (isServer && NetworkManager.singleton.numPlayers == 2 && agents == null)
        {
            agents = GameObject.FindGameObjectsWithTag("Enemy");
        }
	}
    
    public void ResetAllAgentTargets(NetworkInstanceId id)
    {
        for (int i = 0; i < agents.Length; i++)
        {
            agents[i].GetComponent<Agent>().Retarget(id);
        }
    }
}
