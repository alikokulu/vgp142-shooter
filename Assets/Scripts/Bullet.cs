﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter(Collision c)
	{
		Health h = (Health) c.gameObject.GetComponent<Health> ();
		if (h != null) 
		{
			h.TakeDamage (10);
		}
	
	}
}
