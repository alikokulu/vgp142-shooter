﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour
{

	Animator a;

	[SyncVar]
	public float fwd ;

	[SyncVar]
	public float right ;


	Rigidbody rBody ;
	public ParticleSystem fire1;
	public ParticleSystem fire2;
	public GameObject projectile;

	// Use this for initialization
	void Start ()
	{
		rBody = GetComponent<Rigidbody> ();
		a = gameObject.GetComponentInChildren<Animator> ();
	}

	public override void OnStartLocalPlayer()
	{
		gameObject.GetComponentInChildren<Camera> ().enabled = true;
		gameObject.GetComponentInChildren<AudioListener> ().enabled = true;
	}



	// Update is called once per frame
	void Update ()
	{
		HandleAnimation ();

		if (!isLocalPlayer)
        {
            return;
		}

		GetInput ();
		if (Input.GetMouseButtonDown (0)) 
		{
			CmdFire ();
		}
	}

	void FixedUpdate()
	{
		Run ();
		Turn ();
	}

	[Command]
	void CmdFire()
	{
		//Play Particle Effect
		fire1.Play();
		fire2.Play();
		GameObject bullet = (GameObject)Instantiate (projectile, fire1.transform.position + fire1.transform.forward * 0.5f, fire1.transform.rotation);
		bullet.GetComponent<Rigidbody> ().velocity = bullet.transform.forward * 25;
		NetworkServer.Spawn (bullet);
		Destroy (bullet, 2);
	}

	void GetInput()
	{
		    right = Input.GetAxis ("Horizontal");
		    fwd = Input.GetAxis ("Vertical");     
    }

	void HandleAnimation()
	{
		a.SetFloat("RL",right);
		a.SetFloat("FB",fwd);
	}

	void Run()
	{
		rBody.velocity = transform.forward * fwd * 5;
	}

	void Turn()
	{
		transform.rotation *= Quaternion.AngleAxis (100 * right * Time.deltaTime, Vector3.up);
	}
}
