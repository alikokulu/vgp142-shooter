﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class EnemySpawner : NetworkBehaviour
{

	public GameObject enemyPrefab;
	public Transform[] spawnPoints;
	public int numEnemies;

	public override void OnStartServer()
	{
		for (int i = 0; i < numEnemies; i++) 
		{
			var enemy = (GameObject)Instantiate (enemyPrefab, spawnPoints [Random.Range (0, spawnPoints.Length - 1)].position, Quaternion.identity);
			NetworkServer.Spawn (enemy);
		}

	}


	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
