﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Agent : NetworkBehaviour
{

    private int targetPlayer;
    private NavMeshAgent agent;
    private Main mainScript;
    private GameObject[] players;

    // Use this for initialization
    void Start()
    {
        mainScript = GameObject.Find("GameManager").GetComponent<Main>();
        agent = GetComponent<NavMeshAgent>();
        targetPlayer = Random.Range(0, 2);

    }

    // Update is called once per frame
    void Update()
    {
        if (isServer && NetworkManager.singleton.numPlayers == 2)
        {
            if (players == null)
            {
                players = GameObject.FindGameObjectsWithTag("Player");
            }

            agent.destination = players[targetPlayer].transform.position;
        }
    }


    void OnTriggerStay(Collider c)
    {
        if (c.transform.tag != "Player" || !isServer)
            return;

        Health h = (Health)c.gameObject.GetComponent<Health>();
        if (h != null)
        {
            if (h.currentHealth <= Time.deltaTime * 10)
            {
                
                mainScript.ResetAllAgentTargets(h.netId);
            }

            h.TakeDamage(Time.deltaTime * 10);
        }

    }

    public void Retarget(NetworkInstanceId id)
    {
        if (players[targetPlayer].GetComponent<Health>().netId == id)
        {
            targetPlayer = Random.Range(0, 2);
        }
    }
}
